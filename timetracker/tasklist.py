# This file is part of TimeTracker.
#
# TimeTracker is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# TimeTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TimeTracker; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
# Copyright 2006-2007 Julio Biason

'Task list management'
__revision__ = '0.5'

import logging
import datetime
import sys

from task import Task

FIELDS = ('date', 'client', 'reference', 'activity', 'comment')

class TaskListError(Exception):
    '''General tasklist exception'''
    pass

class UnknownFieldError(TaskListError):
    '''The field passed to the summary function is not valid.'''
    def __init__(self, field = None):
        self.field = field

    def __str__(self):
        return self.field

def _add_time(counted_time, hours, minutes, seconds):
    '''Add the time in the array of time.'''
    __hours = counted_time[0]
    __minutes = counted_time[1]
    __seconds = counted_time[2]

    __seconds += seconds
    if __seconds > 59:
        __minutes += 1
        __seconds -= 60

    __minutes += minutes
    if __minutes > 59:
        __hours += 1
        __minutes -= 60

    __hours += hours

    return [__hours, __minutes, __seconds]

class TaskList:
    'List of tasks'

    def __init__ (self):
        '''
        Default constructor.

        Returns:
        Nothing.
        '''

        self.filename  = ''
        self.task_list = []

    def load (self, filename):
        '''
        Load a task file.

        Parameters:
        - filename: the datafile to be loaded.

        Returns:
        Boolean, file loaded successfully.
        '''

        self.filename = filename
        logging.debug ('Data file: ' + filename)
        try:
            file = open (filename, 'r')
        except IOError:
            # Let's just believe this means the file doesn't exist
            logging.warning ('Data file not found')
            return True
        except:
            # now things got pretty bad
            logging.error ('Error opening file ' + filename)
            return False

        lines = file.readlines ()
        logging.debug ('%d lines read' % len (lines))
        file.close ()

        for line in lines[1:]:        # first row = header (just jump it)
            line = line.strip ('\n \t')
            fields = line.split ('\t')

            logging.debug (line)

            try:
                date      = fields [0]
                logging.debug ('Date:' + date)

                start     = fields [1]
                logging.debug ('Start: ' + start)

                end       = fields [2]
                logging.debug ('End: ' + end)

                elapsed   = fields [3]
                logging.debug ('Elapsed' + elapsed)

                client    = fields [4]
                logging.debug ('Client: ' + client)

                activity  = fields [5]
                logging.debug ('Activity: ' + activity)

                reference = fields [6]
                logging.debug ('Reference: ' + reference)

                comment   = fields [7]
                logging.debug ('Comment: ' + comment)
            except:
                logging.error ('Data file corrupted')
                return False

            task = Task ()
            task.set (date, start, end, client, activity, reference, comment)
            self.task_list.append (task)

        return True

    def list (self):
        '''
        Return a list of the tasks.

        Returns:
        An array with all the tasks. The first line contains the headings of
        all following fields.
        '''

        result = []
        result.append ({
                'date': 'Date',
                'start': 'Start time',
                'stop': 'End time',
                'elapsed': 'Duration',
                'client': 'Client',
                'activity': 'Activity',
                'reference': 'Reference',
                'comment': 'Comment'})

        for task in self.task_list:
            result.append ({'date': task.get_date(),
                    'start': task.get_start (),
                    'stop': task.get_stop (),
                    'elapsed': task.get_elapsed (True),
                    'client': task.get_client (),
                    'activity': task.get_activity (),
                    'reference': task.get_reference (),
                    'comment': task.get_comment ()
                    })

        return result

    def last_task_open (self):
        '''
        Check if the last task is still running.
        
        Returns:
        True if the last added task is still active; False
        otherwise.
        '''
        # as we keep every open task in the top of the list, there is
        # no need to search through the list -- it is always the first.
        try:
            task = self.task_list [0]
            return task.is_running ()
        except:
            return False    # there is no task

    def stop_task (self, time = ''):
        '''
        Stop the current task.

        Parameters:
        - time: time of stop; if empty, the current time
          will be used.

        Returns:
        Nothing.
        '''
        try:
            task = self.task_list [0]
            task.stop_task (time)
        except:
            return        # no task to stop

    def start_task (self, client, activity, reference, comment, time = ''):
        '''
        Start a new task.
        
        Returns:
        True if the task could be created, False otherwise.
        '''
        task = Task ()
        if not task.start_task (time):
            # it really should throw an exception, so we
            # could report it upstream, leaving the error
            # messages to the interface
            return False

        task.set_client (client)
        task.set_activity (activity)
        task.set_reference (reference)
        task.set_comment (comment)
        self.task_list.insert (0, task)
        return True

    def save (self):
        '''
        Save the tasklist.
        
        Returns:
        True if the task file could be saved; False otherwise.
        '''
        if self.filename == '':
            logging.error ('Tasks file not open -- cannot save')
            return False

        try:
            output = open (self.filename, 'w')
        except:
            logging.error ('Error opening ' + self.filename + ' to save')
            return False

        try:
            output.write ('Date\tStart time\tEnd time\tDuration\tClient\tActivity\tReference\tComment\n')
        except:
            logging.error ('Error writing header to ' + self.filename)
            return False

        for task in self.task_list:
            output.write (task.get_date () + '\t' +
                    task.get_start () + '\t' +
                    task.get_stop () + '\t' +
                    task.get_elapsed (False) + '\t' +
                    task.get_client () + '\t' +
                    task.get_activity () + '\t' +
                    task.get_reference () + '\t' +
                    task.get_comment () + 
                    '\n')

        output.close ()

    def summary(self, fields):
        '''
        Do a summary, using the field list.
        '''
        field_list = fields.split(',')

        for field in field_list:
            if not field in FIELDS:
                raise UnknownFieldError(field)

        result = {}
        column_sizes = [0, 0, 0, 0, 0]  # 5 fields

        for task in self.task_list:
            # build the key 
            # TODO: find a better way to do this -- this is butt ugly
            key = []
            for field in field_list:
                if field == 'date':
                    field_value = task.get_date('%Y/%m/%d')
                elif field == 'client':
                    field_value = task.get_client()
                elif field == 'reference':
                    field_value = task.get_reference()
                elif field == 'activity':
                    field_value = task.get_activity()
                elif field == 'comment':
                    field_value = task.get_comment()

                column_sizes[len(key)] = max(column_sizes[len(key)], \
                        len(field_value))
                key.append(field_value)

            # add the time
            if result.has_key(tuple(key)):
                run_time     = result[tuple(key)][0]
                elapsed_time = result[tuple(key)][1]
            else:
                run_time     = [0, 0, 0]
                elapsed_time = [0, 0, 0]

            elapsed = task.get_elapsed(True)

            if elapsed[0] == '(':
                # tasks running will be marked with the '('
                has_ended = False
                elapsed = elapsed [1:-1]
            else:
                has_ended = True

            elapsed_fields = elapsed.split(':')
            hours   = int(elapsed_fields[0])
            minutes = int(elapsed_fields[1])
            seconds = int(elapsed_fields[2])

            if not has_ended:
                elapsed_time = _add_time(elapsed_time, hours, minutes,
                        seconds)
            else:
                run_time = _add_time(run_time, hours, minutes, seconds)

            result[tuple(key)] = [run_time, elapsed_time]

        header = []
        for field in xrange(len(field_list)):
            this_size = column_sizes[field]
            header.append(field_list[field].capitalize()[:this_size].ljust(this_size))

        header.append('Complete')
        header.append('Elapsed')

        print ' '.join(header)

        for (key, time) in sorted(result.iteritems()):
            line = []
            for elem in xrange(len(key)):
                line.append(key[elem].ljust(column_sizes[elem]))

            line.append(':'.join([str(x).rjust(2, '0') for x in time[0]]))
                
            if (time[1][0] + time[1][1] + time[1][2]) > 0:
                # still running time
                running_time = _add_time(time[0], time[1][0], time[1][1],
                        time[1][2])
                line.append(':'.join([str(x).rjust(2, '0') for x in
                    running_time]))

            print ' '.join(line)

    def day_summary (self):
        '''
        Do a summary of working hours by day.
        
        Returns:
        Nothing.
        '''

        # result will be a dictionary with two columns: the
        # first one will hold the known time (tasks with end);
        # the second column will have the elapsed time (tasks
        # without end)
        result = {}

        total_seconds = 0
        total_minutes = 0
        total_hours   = 0

        for task in self.task_list:
            date    = task.get_date()
            elapsed = task.get_elapsed (True)

            # tasks without end will have elapsed time begining with
            # a '('
            if elapsed [0] == '(':
                has_ended = False
                elapsed = elapsed [1:-1]
            else:
                has_ended = True

            elapsed_fields = elapsed.split (':')
            hours   = int (elapsed_fields [0])
            minutes = int (elapsed_fields [1])
            seconds = int (elapsed_fields [2])

            try:
                run_time  = result [date][0]
                open_time = result [date][1]
            except:
                run_time  = [0, 0, 0]
                open_time = [0, 0, 0]

            # it must exist something easier to work with
            # times
            if has_ended:
                run_time [2]  = run_time [2] + seconds
                if run_time [2] > 59:
                    run_time [2] = run_time [2] - 60
                    run_time [1] = run_time [1] + 1

                run_time [1] = run_time [1] + minutes
                if run_time [1] > 59:
                    run_time [1] = run_time [1] - 60
                    run_time [0] = run_time [0] + 1

                run_time [0] = run_time [0] + hours
            else:
                open_time [2] = open_time [2] + seconds
                if open_time [2] > 59:
                    open_time [2] = open_time [2] - 60
                    open_time [1] = open_time [1] + 1

                open_time [1] = open_time [1] + minutes
                if open_time [1] > 59:
                    open_time [1] = open_time [1] - 60
                    open_time [0] = open_time [0] + 1

                open_time [0] = open_time [0] + hours

            total_seconds = total_seconds + seconds
            if total_seconds > 59:
                total_seconds = total_seconds - 60
                total_minutes = total_minutes + 1

            total_minutes = total_minutes + minutes
            if total_minutes > 59:
                total_minutes = total_minutes - 60
                total_hours   = total_hours   + 1

            total_hours = total_hours + hours

            result [date] = [run_time, open_time]

        print '%10s\t%8s\t%s' % ('Date', 'Time', '(Time+Elapsed)')
        for date, time in sorted (result.iteritems ()):
            seconds_elapsed = 0
            minutes_elapsed = 0
            hours_elapsed   = 0

            seconds_elapsed = time[0][2] + time [1][2]
            if seconds_elapsed > 59:
                seconds_elapsed = seconds_elapsed - 60
                minutes_elapsed = minutes_elapsed + 1

            minutes_elapsed = minutes_elapsed + time [0][1] + time [1][1]
            if minutes_elapsed > 59:
                minutes_elapsed = minutes_elapsed - 60
                hours_elapsed   = hours_elapsed + 1

            hours_elapsed = hours_elapsed + time [0][0] + time [1][0]

            print '%10s\t%02d:%02d:%02d\t(%02d:%02d:%02d)' % (date, time[0][0], time[0][1], time[0][2], hours_elapsed, minutes_elapsed, seconds_elapsed)

        print '%10s\t%8s\t %02d:%02d:%02d' % ('', '', total_hours, total_minutes, total_seconds)
        return
