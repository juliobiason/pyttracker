# This file is part of TimeTracker.
#
# TimeTracker is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# TimeTracker is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with TimeTracker; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
# Copyright (C) 2006-2007 Julio Biason

'''Single task management'''
__revision__ = '0.3'

import datetime
import logging

def datetime_from_str (date_str, time_str):
    '''
    Converts a string pair with date/time to a datetime object

    Parameters:
    - date_str: String with the date, in format "dd/mm/yyyy"
    - time_str: String with time, in format "hh:mm:ss"

    Returns:
    A new datetime object with the date

    Note: date can use "/", "." or "-" as separator.

    Note: This function is used to interpret the results from the
    file.
    '''

    if date_str == '""' or time_str == '""':
        # empty strings
        return None

    # several delimiters
    if date_str.find ('/') >= 0:
        date = date_str.split ('/')
    elif date_str.find ('.') >= 0:
        date = date_str.split ('.')
    elif date_str.find ('-') >= 0:
        date = date_str.split ('-')

    time = time_str.split (':')
    result = datetime.datetime (int (date [2]), int (date [1]), int (date[0]), int (time [0]), int (time [1]), int (time[2]))
    return result

def empty_string (string):
    '''
    If the string passed is empty, returns "".

    Parameters:
    - string: the string to be checked.

    Returns:
    "" if the string is empty, or the same string if not.

    Notes:
    Although it can look stupid, this is needed to keep
    compatibility with the old TimeKeeper files
    '''

    if string == '':
        return '""'
    else:
        return string

class Task:
    '''
    Task management.
    '''

    def __init__ (self):
        'Constructor'
        self.start     = datetime.datetime (datetime.MINYEAR, 1, 1)
        self.end       = datetime.datetime (datetime.MINYEAR, 1, 1)
        self.client    = ''
        self.activity  = ''
        self.reference = ''
        self.comment   = ''
        self.has_start = False
        self.has_end   = False

    def set (self, date_str, start_str, end_str, client, activity, reference, comment):
        '''
        Set all values about the task

        Parameters:
        - date_str: Date of the task.
        - start_str: Time of the start of the task.
        - end_str: Time of the end of the task.
        - client: Client.
        - activity: Activity.
        - reference: Reference.
        - comment: Comment.

        Returns:
        Nothing.
        '''

        self.start     = datetime_from_str (date_str, start_str)
        self.client    = client
        self.activity  = activity
        self.reference = reference
        self.comment   = comment
        self.has_start = True

        if len (end_str) > 0 and end_str != '""':
            self.end     = datetime_from_str (date_str, end_str)
            self.has_end = True
        else:
            self.end     = datetime.datetime (datetime.MINYEAR, 1, 1)
            self.has_end = False

        return

    def start_task (self, time = ''):
        '''
        Set the start time of the task to the current time.

        Returns:
        Nothing.
        '''

        time = time.strip (' ')
        if len (time) == 0:
            self.start = self.start.now ()
        else:
            if time.find (' ') > 0:
                # includes the date
                temp = time.split (' ')
                date = temp [0].split ('/')
                time = temp [1].split (':')

                if len (time) == 2:
                    time.append ('0')

                try:
                    self.start = self.start.replace (int (date [0]),
                            int (date [1]),
                            int (date [2]),
                            int (time [0]),
                            int (time [1]),
                            int (time [2]))
                except:
                    logging.error ('Invalid date format')
                    return False

            else:
                # just the time
                temp = time.split (':')
                if len (temp) == 2:
                    temp.append ('0')

                try:
                    self.start = self.start.now ()
                    self.start = self.start.replace (self.start.year,
                            self.start.month,
                            self.start.day,
                            int (temp [0]),
                            int (temp [1]),
                            int (temp [2]))
                except:
                    logging.error ('Invalid date format')

        self.has_start = True
        return True

    def stop_task (self, time = ''):
        '''
        Set the stop time of the task to the current time.

        Parameters:
        - time: time of stop; if empty, the current time will be
          used.

        Returns:
        True if the task can be stopped (e.g. it has a start time);
        False if the task can't be stopped (e.g. it doesn't have a start time).
        '''

        if not self.has_start:
            logging.error ('Cannot stop a task that hasn\'t a start')
            return False

        time = time.strip ()
        if len (time) == 0:
            self.end = self.end.now ()
        else:
            logging.debug ('Time to stop:' + time)
            if time.find (' ') >= 0:
                time = time[time.find (' '):]

            time = time.split (':')
            if len (time) == 2:
                time.append ('0')

            try:
                self.end = self.end.now ()
                self.end = self.end.replace (self.end.year,
                        self.end.month,
                        self.end.day,
                        int (time [0]),
                        int (time [1]),
                        int (time [2]))
            except:
                # maybe we could pick the correct exception?
                logging.error ('Invalid date format')
                return False

        self.has_end = True
        return True

    def set_client (self, client):
        '''
        Set the task client.

        Parameters:
        - client: Client to be set in the task.

        Returns:
        Nothing.
        '''

        self.client = client
        return

    def set_activity (self, activity):
        '''
        Set the task activity.

        Parameters:
        - activity: Activity to be set in the task.

        Returns:
        Nothing.
        '''

        self.activity = activity
        return

    def set_reference (self, reference):
        '''
        Set the task reference.

        Parameters:
        - reference: Reference to be set in the task.

        Returns:
        Nothing.
        '''

        self.reference = reference
        return

    def set_comment (self, comment):
        '''
        Set the task comment.

        Parameters:
        - comment: Comment to be set in the task.

        Returns:
        Nothing.
        '''

        self.comment = comment
        return

    def get_date (self, format='%d/%m/%Y'):
        '''
        Return the date of the task.

        Parameters:
        - format: Format of the output, defaults to '%d/%m/%Y'.

        Returns:
        Date, as string, in the format specified.
        '''

        return self.start.strftime (format)

    def get_start (self, format='%H:%M:%S'):
        '''
        Return the starting time of the task.

        Parameters:
        - format; Format of the output, defaults to '%H:%M:%S'.

        Returns:
        The starting time, as string, in the format specified.
        '''
        return self.start.strftime (format)

    def get_stop (self, format='%H:%M:%S'):
        '''
        Return the time of the end of the task.

        Parameters:
        - format: Format of the output, defaults to '%H:%M:%s'.

        Returns:
        The time of the stop of the task, as string, in the format
        specified.
        '''

        if self.has_end:
            return self.end.strftime (format)
        else:
            return '""'

    def get_elapsed (self, show_empty = False):
        '''
        Return the elapsed time of the task.

        Parameters:
        - show_empty: If true and the task still running, show the
          elapsed time since start to the current time. If false and
          the task still running, returns "". 

        Returns:
        The elapsed time of the task.
        '''
        if self.has_end:
            elapsed_delta = self.end - self.start    # a timedelta
            elapsed = elapsed_delta.__str__()
        else:
            if show_empty:
                elapsed_delta = datetime.datetime.now () - self.start
                elapsed_str = elapsed_delta.__str__ ()
                elapsed_str = elapsed_str[:elapsed_str.find('.')]
                elapsed = '(' + elapsed_str + ')'
            else:
                elapsed = '""'

        if elapsed.find ('.') > 1:
            elapsed = elapsed [:elapsed.find ('.')]

        return elapsed

    def get_client (self):
        '''
        Return the client of the task.

        Returns:
        The client of the task or "" if isn't set.
        '''
        return empty_string (self.client)

    def get_activity (self):
        '''
        Return the activity of the task.

        Returns:
        The activity of the task or "" if isn't set.
        '''
        return empty_string (self.activity)

    def get_reference (self):
        '''
        Return the reference of the task.

        Returns:
        The reference of the task or "" if isn't set.
        '''
        return empty_string (self.reference)

    def get_comment (self):
        '''
        Return the comment of the task.

        Returns:
        The comment of the task or "" is isn't set.
        '''
        return empty_string (self.comment)

    def is_running (self):
        '''
        Check if the task still active.

        Returns:
        True of the task hasn't a stop time, False otherwise.
        '''
        return (self.has_end == False)
