from distutils.core import setup
setup(name='timetracker',
        version='1.0.3',
        description='Time tracking application based on TimeKeeper',
        author='Julio Biason',
        author_email='julio.biason@gmail.com',
        url='http://slowhome.org/projects/timetracker',
        packages=['timetracker'],
        scripts=['ttracker'],
        license='GPL',
        )
